#include <iostream>
using namespace std;
/*крестики-нолики*/

void CreateField(char (&field)[5][5]);
void outputField(char (&field)[5][5]);

int main() {
  
  char field [5] [5];

  CreateField(field);
  outputField(field);
  /*
  клетка массива - клетка ввода в игре
  0 0 = 1
  0 2 = 2
  0 4 = 3
  2 0 = 4
  2 2 = 5
  2 4 = 6
  4 0 = 7
  4 2 = 8
  4 4 = 9
  */
  char turn;
  char player[2] = {'X', 'O'};
  char nowTurn; int i=0;
  bool game = true;

  while(game)
  {
    nowTurn = player[i];
    cout << "Turn " << nowTurn << "(1-9)";
    cin >> turn;
    switch(turn)
    {
      case'1':{
        field[0][0] = nowTurn;
        break;
      }
      case'2':{
        field[0][2] = nowTurn;
        break;
      }
      case'3':{
        field[0][4] = nowTurn;
        break;
      }
      case'4':{
        field[2][0] = nowTurn;
        break;
      }
      case'5':{
        field[2][2] = nowTurn;
        break;
      }
      case'6':{
        field[2][4] = nowTurn;
        break;
      }
      case'7':{
        field[4][0] = nowTurn;
        break;
      }
      case'8':{
        field[4][2] = nowTurn;
        break;
      }
      case'9':{
        field[4][4] = nowTurn;
        break;
      }
    }
  
     outputField(field);

    if((field[0][0] == nowTurn && field[0][2] == nowTurn && field[0][4] == nowTurn) 
    || 
    (field[2][0] == nowTurn &&field[2][2] == nowTurn &&field[2][4] == nowTurn) 
    || 
    (field[4][0] == nowTurn &&field[4][2] == nowTurn &&field[4][4] == nowTurn)
    || 
    (field[0][0] == nowTurn &&field[2][0] == nowTurn &&field[4][0] == nowTurn)
    || 
    (field[0][2] == nowTurn &&field[2][2] == nowTurn &&field[4][2] == nowTurn)
    || 
    (field[0][4] == nowTurn &&field[2][4] == nowTurn &&field[4][4] == nowTurn)
    || 
    (field[0][0] == nowTurn &&field[2][2] == nowTurn &&field[4][4] == nowTurn)
    || 
    (field[0][4] == nowTurn &&field[2][2] == nowTurn &&field[4][0] == nowTurn)
    )
    
    {
      cout << nowTurn << " win.";
      game = false;
      continue;
    }

    if(i == 0) i = 1; else i = 0;
  }
}

void outputField(char (&field)[5][5])
{
  for(int i=0; i < 5; i++)
    {
      for(int j=0; j < 5; j++)
      {
        cout << field[i][j];
      }
      cout << endl;
    }
}

void CreateField(char (&field)[5][5])
{
  for(int i=0; i < 5; i++)
  {
    for(int j=0; j < 5; j++)
    { 
      if(j%2) field[i][j] = '|';
      else if(i%2) field[i][j] = '-';
      else field[i][j] = ' '; 
    }
  }
}